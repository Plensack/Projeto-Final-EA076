/*  Projeto Final - EA076 - Turma D - 1s2018
 *  Alunos: 
 *  Gustavo Granela Plensack - RA:155662
 *  Guilherme Rosa - RA 157955 
 */
//bibliotecas
#include "TimerOne.h" // biblioteca que simplifica a configuração do timer
 
//defines
#define base 1000//base de 1ms
#define OEC6 3
#define RClk 4
#define SClk 5
#define SDataC 6
#define OEK 7
#define SDataK  8
#define OEC1 9
#define OEC2 10
#define OEC3 11
#define OEC4 12
#define OEC5 13


//funções
void iniciaGPIO();
void interruptOn(); //ativa a interrução do timer 1
void interruptOff(); // desativa a interrupçaõ do timer 1
void writeLayer(int layer2Light);
void refreshCube();
void writeCube();
void enableColumn(int column2Enable);

//efeitos do cubo
void acende1();

//variaveis e objetos globais
int nextCube[6*6*6] = {0}; // esse vetor de 6^3 variaveis tem o objetivo de armazenar o que será escrito em cada LED
int currentCube[6*6*6] = {0};
/* Faixa de Indices -> Camada
 * 000 - 035 -> 0 (INFERIOR)
 * 036 - 071 -> 1
 * 072 - 107 -> 2
 * 108 - 143 -> 3
 * 144 - 179 -> 4
 * 180 - 215 -> 5 (SUPERIOR)
 * 0 indica apagado e 1 indica aceso
 */
int timeCounter = 0;
int refreshCounter = 0;

void setup(){
  Serial.begin(9600);
  iniciaGPIO();
  interruptOn();
  
  
}
int a = 0;
int layer = 0;
void loop(){
  
  if(refreshCounter>50){
    refreshCounter = 0;
    a++;
    if(a==6) a = 0;
    enableColumn(a);
    }
  if(timeCounter>500){
    timeCounter = 0;
//    nextCube[a] = 1;
    }
    
}

void iniciaGPIO(){
  for (int i = 3; i<=13 ;i++){
    pinMode(i,OUTPUT);
    }
    digitalWrite(OEK,LOW);
    digitalWrite(OEC1,HIGH);
    digitalWrite(OEC2,HIGH);
    digitalWrite(OEC3,HIGH);
    digitalWrite(OEC4,HIGH);
    digitalWrite(OEC5,HIGH);
    digitalWrite(OEC6,HIGH);
}


/*para o 74595 será enviada e armazenada a seguinte sequencia:
  1xxxxxx1 - o comando dos leds que serão acesos estão no meio destes valores:
  1 -> Aceso
  0 -> Apagado
  Se esse valor significará um HIGH ou LOW, depende do tipo do transistor associado (NPN ou PNP)
  para o NPN: HIGH = Aceso e LOW = Apagado
  para o PNP: HIGH = Apagado e LOW = Aceso
*/

void refreshCube(){
  for(int i = 0; i<216;i++){
    currentCube[i] = nextCube[i];
  //  Serial.print(currentCube[i]);
    }
  //  Serial.println("");
  }

void writeCube(){
  refreshCube();
  int seq74595[8] = {0};
  int i = 0;
  seq74595[0] = 0;
  seq74595[1] = currentCube[i*36+0];
  seq74595[2] = currentCube[i*36+1];
  seq74595[3] = currentCube[i*36+2];
  seq74595[4] = currentCube[i*36+3];
  seq74595[5] = currentCube[i*36+4];
  seq74595[6] = currentCube[i*36+5];
  seq74595[7] = 0;

  Serial.println();
    for(int j=0;j<8;j++){
      Serial.print(seq74595[j]);
      if(seq74595[j] == 1){
        digitalWrite(SDataC,LOW);  
        }else{
        digitalWrite(SDataC,HIGH);  
        }
      delayMicroseconds(1);
      digitalWrite(SClk,LOW);
      delayMicroseconds(1);
      digitalWrite(SClk,HIGH);
    }
    delayMicroseconds(1);
    digitalWrite(RClk,LOW);
    delayMicroseconds(1);
    digitalWrite(RClk,HIGH);
    enableColumn(0);
    delay(50);
  }

void enableColumn(int column2Enable){
  if(column2Enable == 0){
    digitalWrite(OEC1,LOW);
    digitalWrite(OEC2,HIGH);
    digitalWrite(OEC3,HIGH);
    digitalWrite(OEC4,HIGH);
    digitalWrite(OEC5,HIGH);
    digitalWrite(OEC6,HIGH);
    }
  
  if(column2Enable == 1){
    digitalWrite(OEC1,HIGH);
    digitalWrite(OEC2,LOW);
    digitalWrite(OEC3,HIGH);
    digitalWrite(OEC4,HIGH);
    digitalWrite(OEC5,HIGH);
    digitalWrite(OEC6,HIGH);
    }  
   
   if(column2Enable == 2){
    digitalWrite(OEC1,HIGH);
    digitalWrite(OEC2,HIGH);
    digitalWrite(OEC3,LOW);
    digitalWrite(OEC4,HIGH);
    digitalWrite(OEC5,HIGH);
    digitalWrite(OEC6,HIGH);
    }

  if(column2Enable == 3){
    digitalWrite(OEC1,HIGH);
    digitalWrite(OEC2,HIGH);
    digitalWrite(OEC3,HIGH);
    digitalWrite(OEC4,LOW);
    digitalWrite(OEC5,HIGH);
    digitalWrite(OEC6,HIGH);
    }
  
  if(column2Enable == 4){
    digitalWrite(OEC1,HIGH);
    digitalWrite(OEC2,HIGH);
    digitalWrite(OEC3,HIGH);
    digitalWrite(OEC4,HIGH);
    digitalWrite(OEC5,LOW);
    digitalWrite(OEC6,HIGH);
    }

  if(column2Enable == 5){
    digitalWrite(OEC1,HIGH);
    digitalWrite(OEC2,HIGH);
    digitalWrite(OEC3,HIGH);
    digitalWrite(OEC4,HIGH);
    digitalWrite(OEC5,HIGH);
    digitalWrite(OEC6,LOW);
    }
    
  }

  
void selectLayer(int layer2Light){
  int seq74595[8] = {0};
  seq74595[layer2Light+1] = 1;
  for(int i = 0; i<8;i++){
    if(seq74595[i] == 1){
      digitalWrite(SDataK,HIGH);  
      }else{
      digitalWrite(SDataK,LOW);  
      }
    delayMicroseconds(1);
    digitalWrite(SClk,LOW);
    delayMicroseconds(1);
    digitalWrite(SClk,HIGH);
    }
    delayMicroseconds(1);
    digitalWrite(RClk,LOW);
    delayMicroseconds(1);
    digitalWrite(RClk,HIGH);
  }


void increaseTime(){//funcao de interrupcao implementamos 1 timer
 refreshCounter++;
 timeCounter++;
   if(layer==6){ 
    layer = 0;
    }
  selectLayer(layer);
  layer++;
 }

void interruptOn(){//liga as interrupcoes do timerOne
  Timer1.initialize(base);
  Timer1.attachInterrupt(increaseTime);
  }
  
void interruptOff(){//desliga as interrupcoes do timerOne
  Timer1.detachInterrupt(); 
  }