/*  Projeto Final - EA076 - Turma D - 1s2018
 *  Alunos: 
 *  Gustavo Granela Plensack - RA:155662
 *  Guilherme Rosa - RA 157955 
 */
/* Faixa de Indices -> Camada
 * 000 - 035 -> 0 (INFERIOR)
 * 036 - 071 -> 1
 * 072 - 107 -> 2
 * 108 - 143 -> 3
 * 144 - 179 -> 4
 * 180 - 215 -> 5 (SUPERIOR)
 * 0 indica apagado e 1 indica aceso
*/



//definies e bibliotecas
#include "TimerOne.h"
#define latchPinC6 3
#define SClk 5 
#define OE 4
#define SDataC 6
#define latchPinK 7
#define SDataK  8
#define latchPinC1 9
#define latchPinC2 10
#define latchPinC3 11
#define latchPinC4 12
#define latchPinC5 13

//variaveis
int timeCounter = 0;
int refreshCounter = 0;
int layerLEDs[36] = {0};

//funções
void iniciaGPIO();
void armazenaColuna(int col);
void clkCPulse();
void clkKPulse();
void insereImagem();
void escolheLayer(int layer);

void setup(){
  iniciaGPIO();
  digitalWrite(OE,LOW);
  for(int i = 0; i<36;i++){
  layerLEDs[i] = 1;
  insereImagem();
  escolheLayer(0);
  delay(5*100);
}
}
void loop(){
 
}

void escolheLayer(int layer){
   
   int str[8] = {0};
    str[layer+1] = 1;
 
    for (int i = 0;i<8;i++){
    if(str[i] == 1){
      digitalWrite(SDataK,HIGH);
      } else {
        digitalWrite(SDataK,LOW);
        }
    clkCPulse();
    }
  delayMicroseconds(1);
  digitalWrite(latchPinK,HIGH);
  delayMicroseconds(1);
  digitalWrite(latchPinK,LOW);
  delayMicroseconds(1);  
  
  digitalWrite(OE,LOW);
  
  }



void insereImagem(){
   int str[8] = {0};
  for (int j=0;j<6;j++){
    str[0] = 0;
    str[1] = layerLEDs[0+6*j];
    str[2] = layerLEDs[1+6*j];
    str[3] = layerLEDs[2+6*j];
    str[4] = layerLEDs[3+6*j];
    str[5] = layerLEDs[4+6*j];
    str[6] = layerLEDs[5+6*j];
    str[7] = 0;
    
  for (int i = 0;i<8;i++){
    if(str[i] == 1){
      digitalWrite(SDataC,LOW);
      } else {
        digitalWrite(SDataC,HIGH);
        }
    clkCPulse();
    }
  armazenaColuna(j);
  }
  digitalWrite(OE,LOW);  
}


void iniciaGPIO(){
  pinMode(SDataK,OUTPUT);//SER
  pinMode(OE,OUTPUT);
  pinMode(latchPinK,OUTPUT);//latch
  pinMode(SClk,OUTPUT);//clk
  pinMode(latchPinC6,OUTPUT);
  pinMode(latchPinC5,OUTPUT);
  pinMode(latchPinC4,OUTPUT);
  pinMode(latchPinC3,OUTPUT);
  pinMode(latchPinC2,OUTPUT);
  pinMode(latchPinC1,OUTPUT);  
  pinMode(SDataC,OUTPUT);
}

void armazenaColuna(int col){
  if(col==0){
    delayMicroseconds(1);
    digitalWrite(latchPinC1,HIGH);
    delayMicroseconds(1);
    digitalWrite(latchPinC1,LOW);
    delayMicroseconds(1);  
    }
    
  if(col==1){
    delayMicroseconds(1);
    digitalWrite(latchPinC2,HIGH);
    delayMicroseconds(1);
    digitalWrite(latchPinC2,LOW);
    delayMicroseconds(1);    
  }
  
  if(col==2){
    delayMicroseconds(1);
    digitalWrite(latchPinC3,HIGH);
    delayMicroseconds(1);
    digitalWrite(latchPinC3,LOW);
    delayMicroseconds(1);  
  }
   
  if(col==3){
    delayMicroseconds(1);
    digitalWrite(latchPinC4,HIGH);
    delayMicroseconds(1);
    digitalWrite(latchPinC4,LOW);
    delayMicroseconds(1);  
  }
    
  if(col==4){
    delayMicroseconds(1);
    digitalWrite(latchPinC5,HIGH);
    delayMicroseconds(1);
    digitalWrite(latchPinC5,LOW);
    delayMicroseconds(1);  
  }
    
  if(col==5){
    delayMicroseconds(1);
    digitalWrite(latchPinC6,HIGH);
    delayMicroseconds(1);
    digitalWrite(latchPinC6,LOW);
    delayMicroseconds(1);  
    }  
  
  }
  
void clkCPulse(){
    delayMicroseconds(1);
    digitalWrite(SClk,HIGH);
    delayMicroseconds(1);
    digitalWrite(SClk,LOW);
  }
void clkKPulse();


