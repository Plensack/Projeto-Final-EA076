/*  Projeto Final - EA076 - Turma D - 1s2018
 *  Alunos: 
 *  Gustavo Granela Plensack - RA:155662
 *  Guilherme Rosa - RA 157955 
 */
/* Faixa de Indices -> Camada
 * 000 - 035 -> 0 (INFERIOR)
 * 036 - 071 -> 1
 * 072 - 107 -> 2
 * 108 - 143 -> 3
 * 144 - 179 -> 4
 * 180 - 215 -> 5 (SUPERIOR)
 * 0 indica apagado e 1 indica aceso
 */
#define latchPinC6 3
#define SClk 5 
#define OE 4
#define SDataC 6
#define latchPinK 7
#define SDataK  8
#define latchPinC1 9
#define latchPinC2 10
#define latchPinC3 11
#define latchPinC4 12
#define latchPinC5 13

int timeCounter = 0;
int refreshCounter = 0;

void setup(){
  pinMode(SDataK,OUTPUT);//SER
  pinMode(latchPinK,OUTPUT);//latch
  pinMode(SClk,OUTPUT);//clk

  delay(10);
  int str[8] = {0};

  str[0] = 0;
  str[1] = 1;
  str[2] = 0;
  str[3] = 1;
  str[4] = 0;
  str[5] = 1;
  str[6] = 0;
  str[7] = 1;
  

  for(int i = 0;i<8;i++){
    if(str[i] == 1){
      digitalWrite(SDataK,HIGH);
      } else {
        digitalWrite(SDataK,LOW);
        }
      
    delayMicroseconds(1);
    digitalWrite(SClk,HIGH);
    delayMicroseconds(1);
    digitalWrite(SClk,LOW);
    }
  delayMicroseconds(1);
  digitalWrite(latchPinK,HIGH);
  delayMicroseconds(1);
  digitalWrite(latchPinK,LOW);
  delayMicroseconds(1);
}


void loop(){
      
}