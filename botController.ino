#include "ESP8266WiFi.h" // biblioteca para manuseio do shield ESP8266
#include "WiFiClientSecure.h"
#include "TelegramBot.h"

# define LED D4 // Led embutido na placa
#define RX2 D5
#define TX2 D6
// Nome da Rede e Senha
const char* ssid = "Gustavo Plensack";
const char* senha = "proj_076";
const char BotToken[] = "593351083:AAENuWqUjV-pL7LRPFm1h1tCBt0_esoclx0"; 
WiFiClientSecure net_ssl; 
TelegramBot bot (BotToken, net_ssl); //Classe que armazena o Bot do Telegram
long long int timeAux = 0;
 message m;

void setup(){ 
  pinMode(LED, OUTPUT);//inicia o led da placa como saida
  Serial.begin(9600);
  WiFi.begin(ssid, senha);
   WiFi.mode(WIFI_STA); // SETS TO STATION MODE!
  while (WiFi.status() != WL_CONNECTED) {//aguarda conectar ao WiFi
     delay(500);
     digitalWrite(LED,!digitalRead(LED));
  }
  bot.begin();//inicia o bot do telegram
  
}

void loop() {

  if(millis() - timeAux >= 1000){
    m = bot.getUpdates();
    timeAux = millis();//atualiza o timer  
    }
  
  if(m.text != ""){
     digitalWrite(LED, LOW);
    if (m.text.equals("/rain")){//tratanento do comando Rain
      bot.sendMessage(m.chat_id, "Comando /rain recebido. O cubo irá executar um efeito que simula uma chuva");
      Serial.print("CHUVA*");
    } else if (m.text.equals("/random")){
      bot.sendMessage(m.chat_id, "Comando /random recebido. O cubo irá acender leds aleatoriamente");
      Serial.print("RAND*");
    } else if(m.text.equals("/ledtesting")){
      bot.sendMessage(m.chat_id, "Comando /ledtesting recebido. O cubo irá testar todos os leds, acendendo um à um");
      Serial.print("TST*");
    } else if(m.text.equals("/start")){
      bot.sendMessage(m.chat_id, "Bot iniciado ! Digite / para ver a lista de comandos.");
    } else{
      bot.sendMessage(m.chat_id, "O comando recebido é inválido. Tente novamente.");
    }
      m.text = "";
      digitalWrite(LED, HIGH);
  }
}