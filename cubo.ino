/*  Projeto Final - EA076 - Turma D - 1s2018
 *  Alunos: 
 *  Gustavo Granela Plensack - RA:155662
 *  Guilherme Rosa - RA 157955 
 */
/* Faixa de Indices -> Camada
 * 000 - 035 -> 0 (INFERIOR)
 * 036 - 071 -> 1
 * 072 - 107 -> 2
 * 108 - 143 -> 3
 * 144 - 179 -> 4
 * 180 - 215 -> 5 (SUPERIOR)
 * 0 indica apagado e 1 indica aceso
*/



//definies e bibliotecas
#include "TimerOne.h"
#define latchPinC6 3
#define SClk 5 
#define OE 4
#define SDataC 6
#define latchPinK 7
#define SDataK  8
#define latchPinC1 9
#define latchPinC2 10
#define latchPinC3 11
#define latchPinC4 12
#define latchPinC5 13
#define base 3000

//variaveis
int timeCounter = 0;
int refreshCounter = 0;
int currentCube[216] = {0};
int nextCube[216] = {0};
int currentLayer = 0;
bool allowInterrupt = true;

//funções
void iniciaGPIO();
void armazenaColuna(int col);
void clkCPulse();
void clkKPulse();
void insereImagem();
void escolheLayer(int layer);
void refresh_imagem(); // ISR que é responsável pela sincronização dos eventos no Loop
void timerInterruptOn(); //ativa a interrução do timer 1
void timerInterruptOff(); // desativa a interrupçaõ do timer 1
void refresh_conteudo();
void efeito_sequencial();
void limpa_cubo();
void efeito_random();
void efeito_chuva();

void setup(){
  randomSeed(analogRead(0));
  iniciaGPIO();
  timerInterruptOn();
}

void loop(){
  //efeito_sequencial();
  //limpa_cubo();
  efeito_random();
  limpa_cubo();
  efeito_chuva();
  limpa_cubo();
}

void efeito_chuva(){
  int i = 0;
  int cols_pingos[15] = {21,30,19,28,12,35,23,9,28,2,10,15,22,19,12};
  int layer_pingos[15] = {0};
  
  for(int j = 0;j<50;j++){
  while(layer_pingos[9] != 5){
    layer_pingos[i+0] = 5;
    layer_pingos[i+1] = 4;
    layer_pingos[i+2] = 3;
    layer_pingos[i+3] = 2;
    layer_pingos[i+4] = 1;
    layer_pingos[i+5] = 0;

    nextCube[36*layer_pingos[i+0] + cols_pingos[i+0]] = 1;
    nextCube[36*layer_pingos[i+1] + cols_pingos[i+1]] = 1;
    nextCube[36*layer_pingos[i+2] + cols_pingos[i+2]] = 1;
    nextCube[36*layer_pingos[i+3] + cols_pingos[i+3]] = 1;
    nextCube[36*layer_pingos[i+4] + cols_pingos[i+4]] = 1;
    nextCube[36*layer_pingos[i+5] + cols_pingos[i+5]] = 1;
    
    i++;
    delay(100);
    limpa_cubo();
    } 
  }
}

void efeito_random(){
  int i = 0;
  int index = 0;
  while(i<216){
    nextCube[random(0, 216)] = 1;
    i++;
    delay(50);
    }
    

}  

void limpa_cubo(){
  for(int i = 0; i<216;i++){
    nextCube[i] = 0;
  }
  }




void efeito_sequencial(){
  
  for(int i = 0; i<216;i++){
    nextCube[i] = 1;
    delay(50);
  }
  
}

void refresh_imagem(){//interrupção temporizada a cada 0,001s  
  if(allowInterrupt){
  allowInterrupt = false;
  
  refresh_conteudo();
  if(currentLayer == 7) currentLayer = 0;
  digitalWrite(OE,HIGH);
  insereImagem();
  escolheLayer(currentLayer);  
  digitalWrite(OE,LOW);
  currentLayer++;
  allowInterrupt = true;
  }
}

void refresh_conteudo(){
  for(int i = 0;i<216;i++){
    currentCube[i] = nextCube[i];
    }  
  
  }

void timerInterruptOn(){//liga as interrupcoes do timerOne
  Timer1.initialize(base);
  Timer1.attachInterrupt(refresh_imagem);
  }
  
void timerInterruptOff(){//desliga as interrupcoes do timerOne
  Timer1.detachInterrupt(); 
  }

void escolheLayer(int layer){
   
   int str[8] = {0};
    str[layer+1] = 1;
 
    for (int i = 0;i<8;i++){
    if(str[i] == 1){
      digitalWrite(SDataK,HIGH);
      } else {
        digitalWrite(SDataK,LOW);
        }
    clkCPulse();
    }
  delayMicroseconds(1);
  digitalWrite(latchPinK,HIGH);
  delayMicroseconds(1);
  digitalWrite(latchPinK,LOW);
  delayMicroseconds(1);  
  
  digitalWrite(OE,LOW);
  
  }
  
void insereImagem(){
   int str[8] = {0};
  for (int j=0;j<6;j++){
    str[0] = 0;
    str[1] = currentCube[0+6*j + 36*currentLayer];
    str[2] = currentCube[1+6*j + 36*currentLayer];
    str[3] = currentCube[2+6*j + 36*currentLayer];
    str[4] = currentCube[3+6*j + 36*currentLayer];
    str[5] = currentCube[4+6*j + 36*currentLayer];
    str[6] = currentCube[5+6*j + 36*currentLayer];
    str[7] = 0;
    
  for (int i = 0;i<8;i++){
    if(str[i] == 1){
      digitalWrite(SDataC,LOW);
      } else {
        digitalWrite(SDataC,HIGH);
        }
    clkCPulse();
    }
  armazenaColuna(j);
  }
  digitalWrite(OE,LOW);  
}


void iniciaGPIO(){
  pinMode(SDataK,OUTPUT);//SER
  pinMode(OE,OUTPUT);
  pinMode(latchPinK,OUTPUT);//latch
  pinMode(SClk,OUTPUT);//clk
  pinMode(latchPinC6,OUTPUT);
  pinMode(latchPinC5,OUTPUT);
  pinMode(latchPinC4,OUTPUT);
  pinMode(latchPinC3,OUTPUT);
  pinMode(latchPinC2,OUTPUT);
  pinMode(latchPinC1,OUTPUT);  
  pinMode(SDataC,OUTPUT);
}

void armazenaColuna(int col){
  if(col==0){
    delayMicroseconds(1);
    digitalWrite(latchPinC1,HIGH);
    delayMicroseconds(1);
    digitalWrite(latchPinC1,LOW);
    delayMicroseconds(1);  
    }
    
  if(col==1){
    delayMicroseconds(1);
    digitalWrite(latchPinC2,HIGH);
    delayMicroseconds(1);
    digitalWrite(latchPinC2,LOW);
    delayMicroseconds(1);    
  }
  
  if(col==2){
    delayMicroseconds(1);
    digitalWrite(latchPinC3,HIGH);
    delayMicroseconds(1);
    digitalWrite(latchPinC3,LOW);
    delayMicroseconds(1);  
  }
   
  if(col==3){
    delayMicroseconds(1);
    digitalWrite(latchPinC4,HIGH);
    delayMicroseconds(1);
    digitalWrite(latchPinC4,LOW);
    delayMicroseconds(1);  
  }
    
  if(col==4){
    delayMicroseconds(1);
    digitalWrite(latchPinC5,HIGH);
    delayMicroseconds(1);
    digitalWrite(latchPinC5,LOW);
    delayMicroseconds(1);  
  }
    
  if(col==5){
    delayMicroseconds(1);
    digitalWrite(latchPinC6,HIGH);
    delayMicroseconds(1);
    digitalWrite(latchPinC6,LOW);
    delayMicroseconds(1);  
    }  
  
  }
  
void clkCPulse(){
    delayMicroseconds(1);
    digitalWrite(SClk,HIGH);
    delayMicroseconds(1);
    digitalWrite(SClk,LOW);
  }
void clkKPulse();
