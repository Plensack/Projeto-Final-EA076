#include "ESP8266WiFi.h"
#include "WiFiClientSecure.h"
#include "TelegramBot.h"

# define LED D4 // Use built-in LED which connected to D4 pin or GPIO 2

// WiFi parameters to be configured
const char* ssid = "Fifi";
const char* password = "Capivara";
const char BotToken[] = "593351083:AAENuWqUjV-pL7LRPFm1h1tCBt0_esoclx0"; 
WiFiClientSecure net_ssl; 
TelegramBot bot (BotToken, net_ssl); 

void setup(void)
{ 
  Serial.begin(9600);
  // Connect to WiFi
  WiFi.begin(ssid, password);
  // while wifi not connected yet, print '.'
  // then after it connected, get out of the loop
  while (WiFi.status() != WL_CONNECTED) {
     delay(500);
     Serial.print(".");
  }
  //print a new line, then print WiFi connected and the IP address
  Serial.println("");
  Serial.println("WiFi connected");
  // Print the IP address
  Serial.println(WiFi.localIP());
  Serial.println("Connected to "+String(ssid));
 
  bot.begin();   
  pinMode(LED, OUTPUT);   

}
void loop() {
  message m = bot.getUpdates(); // Read new messages   
if (m.text.equals("/rain"))  
      {   
  digitalWrite(LED, LOW);    
  bot.sendMessage(m.chat_id, "The Led is now ON");   
}   
else if (m.text.equals("/random"))  
      {   
  digitalWrite(LED, HIGH);    
  bot.sendMessage(m.chat_id, "The Led is now OFF");   
}   
}